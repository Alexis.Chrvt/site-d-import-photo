<?php
// lancement de la session
	session_start();
	// définition de l'URL de base pour les redirections
	define('baseURL','http://localhost/authentification/');
	// Si la route est configurée lors de l'appel à cette page,
	// on appelle le bon contrôleur
	if (isset($_GET['route'])) {    
	    switch($_GET['route']) {
	        case 'inscription':
	            require('application/controleurs/authentification.php');
	            inscription();
	            break;
	        case 'connexion':
	            require('application/controleurs/authentification.php');
	            connexion();
	            break;
	        case 'deconnexion':
	            require('application/controleurs/authentification.php');
	            deconnexion();
	            break;

			case 'import':
				require('application/controleurs/ajoutPhoto.php');
					break;

			case 'ajoutVote':
				require('application/controleurs/gestionVotes.php');
				break;
	    }
	} else {
	    // sinon on appelle le contrôleur par défaut
	    require('application/controleurs/accueil.php');
	}
    
	?>
