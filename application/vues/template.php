<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$title?></title>
    <link rel="stylesheet" href="./public/css/template.css">
</head>
<body>
    <!--barre de navigation -->
    <nav>
        <?php
        if (isset($_SESSION['pseudo']))
        {
        ?>
        <a href="index.php">accueil</a>
        Connecté en tant que <?=$_SESSION['pseudo']?>
        <a href="index.php?route=import">importer une image</a>
        <a href="index.php?route=deconnexion">Se déconnecter</a>
        <?php
        }else {
        ?>
        <a href="index.php">accueil</a>
        <a href="index.php?route=connexion">Se connecter</a>
        <a href="index.php?route=inscription">S'inscrire</a>
        <?php
        }
        ?>
    </nav>
    <!-- contenu principal -->
    <main>
        <?php
        echo $content;
        ?>
    </main>
</body>
</html>