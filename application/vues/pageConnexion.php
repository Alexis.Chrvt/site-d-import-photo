<?php ob_start();
?>
<form action="index.php?route=connexion" method="POST">
    <?php
    // si on revient a cette page suite a une erreur de saisie, on affiche le message d'erreur
    if(isset($_SESSION['error'])) {
        echo '<p class=error>' .$_SESSION['error'].'</p>';
        unset($_SESSION['error']);
    }
    ?>
    <fieldset>
    <label for="pseudo">Entrez votre Pseudo</label>
    <input type="text" name="pseudo" id="pseudo">
    <label for="pw">Entrez votre mot de passe</label>
    <input type="password" name="pw" id="pw">
    <button type="submit">Se connecter</button>
</form>
</fieldset>
<?php
    $content = ob_get_clean();
    $title = "Connexion";
    require('application/vues/template.php');
?>
