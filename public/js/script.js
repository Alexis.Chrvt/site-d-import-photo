/* eslint-disable no-console */
const etoiles = document.querySelectorAll('input[type=radio]');

console.log(etoiles);

// Récupération des éléments de l'étoile d'évaluation
const ratingInputs = document.querySelectorAll('input');

ratingInputs.forEach((range) => {
  range.addEventListener('click', (event) => {
    const ev = event;
    console.log(ev.target.parentElement.parentElement.children[1]);
    const { value } = range;
    console.log(`J'ai cliqué sur l'étoile avec une valeur de ${value}.`);
    ev.target.parentElement.parentElement.children[2].textContent = `Note: ${event.target.value}`;

    const id_photo = range.parentElement.parentElement.children[1].children[0].getAttribute('id').substring(6);
    console.log(`L'id de la photo est ${id_photo}`);
    
    const vote = {
      id_photo: id_photo,
      valeur: event.target.value,

    };

    const params = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(vote),
    };

    fetch('index.php?route=ajoutVote', params);
  });
});
