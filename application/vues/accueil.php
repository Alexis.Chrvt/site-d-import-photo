<?php
$title = "accueil";
$content = "";
require('application/modeles/connect.php');
require("application/vues/template.php");

    $db = connect();

    $sth = $db->prepare("SELECT `img_ID`, `imgPath`, `imgAlt`, `imgTitre`, `imgRating` FROM `image`");
    $sth->execute();
    $result = $sth->fetchAll(PDO::FETCH_ASSOC);
?>

<link rel="stylesheet" href="./public/css/accueil.css">
<form action="index.php" method="POST">


    <div class='placementImage'>


        <?php foreach ($result as $ligne) { ?>
        <fieldset>
        <?php echo "<img src=\"./public/media/" . $ligne['imgPath'] . "\" alt=\"" . $ligne['imgAlt'] . "\" />"; ?>
            <div class="arrangement">
                <?php echo "<h2>" . $ligne['imgTitre'] . "</h2>" ?>
                <div class="rating">
                    <?php echo "<input type=\"radio\" id=\"star5-" . $ligne['img_ID'] . "\" name=\"rating" . $ligne['img_ID'] . "\" value=\"5\">"; ?>
                    <?php echo "<label class=\"etoile\" for=\"star5-" . $ligne['img_ID'] . "\"></label>"; ?>

                    <?php echo "<input type=\"radio\" id=\"star4-" . $ligne['img_ID'] . "\" name=\"rating" . $ligne['img_ID'] . "\" value=\" 4\">"; ?>
                    <?php echo "<label class=\"etoile\" for=\"star4-" . $ligne['img_ID'] . "\"></label>"; ?>

                    <?php echo "<input type=\"radio\" id=\"star3-" . $ligne['img_ID'] . "\" name=\"rating" . $ligne['img_ID'] . "\" value=\"3\">"; ?>
                    <?php echo "<label class=\"etoile\" for=\"star3-" . $ligne['img_ID'] . "\"></label>"; ?>

                    <?php echo "<input type=\"radio\" id=\"star2-" . $ligne['img_ID'] . "\" name=\"rating" . $ligne['img_ID'] . "\" value=\"2\">"; ?>
                    <?php echo "<label class=\"etoile\" for=\"star2-" . $ligne['img_ID'] . "\"></label>"; ?>

                    <?php echo "<input type=\"radio\" id=\"star1-" . $ligne['img_ID'] . "\" name=\"rating" . $ligne['img_ID'] . "\" value=\"1\">"; ?>
                    <?php echo "<label class=\"etoile\" for=\"star1-" . $ligne['img_ID'] . "\"></label>"; ?>
                </div>
                <div class="note"> 
                    <?php echo "<p class=\"result\">Note : " . $ligne['imgRating'] . "</p>"; ?>
                </div>
            </div>
        </fieldset>
        <?php } ?>
    </div>
</form>      


<script src="./public/js/script.js"></script>