-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 14 déc. 2023 à 13:22
-- Version du serveur : 10.4.28-MariaDB
-- Version de PHP : 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `authentification`
--

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `img_ID` int(11) NOT NULL,
  `imgPath` varchar(64) NOT NULL,
  `imgAlt` varchar(64) NOT NULL,
  `imgTitre` varchar(64) NOT NULL,
  `imgRating` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `image`
--

INSERT INTO `image` (`img_ID`, `imgPath`, `imgAlt`, `imgTitre`, `imgRating`) VALUES
(7, 'image1.png', 'lune', 'Lune', 0),
(12, 'image2.jpg', 'le bggggggg', 'Forêt', 0),
(14, 'image5.jpg', 'hg', 'Cabane', 0),
(15, 'image4.jpg', 'test', 'Montagne', 0),
(18, 'tanjiro giga badass.PNG', 'Tanjiro', 'Tanjiro', 0),
(19, 'wallpaper-fond-ecran-avril-2022_lebocaldange.jpg', 'Image stylisé', 'Image stylisé', 0),
(21, '2023-02-28_23.18.53.jpg', 'test', 'test', 0);

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE `photo` (
  `id_photo` int(11) NOT NULL,
  `auteur_photo` varchar(128) NOT NULL,
  `description_photo` tinytext NOT NULL,
  `chemin_photo` varchar(128) NOT NULL,
  `date_photo` datetime NOT NULL,
  `titre_photo` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `pseudo_utilisateur` varchar(128) NOT NULL,
  `mail_utilisateur` varchar(128) NOT NULL,
  `mdp_utilisateur` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`pseudo_utilisateur`, `mail_utilisateur`, `mdp_utilisateur`) VALUES
('Alexis', 'alexis.charvet@icloud.com', 'Minecraft456'),
('JeanMich', 'jean@xn--zhnzdzjud-u3a.com', 'azertyuiop'),
('Luzagal', 'luza@gal.com', 'Luza');

-- --------------------------------------------------------

--
-- Structure de la table `vote`
--

CREATE TABLE `vote` (
  `photo_vote` int(11) NOT NULL,
  `utilisateur_vote` varchar(128) NOT NULL,
  `valeur_vote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`img_ID`);

--
-- Index pour la table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id_photo`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`pseudo_utilisateur`,`mail_utilisateur`);

--
-- Index pour la table `vote`
--
ALTER TABLE `vote`
  ADD KEY `photo` (`photo_vote`),
  ADD KEY `utilisateur` (`utilisateur_vote`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `img_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `photo`
--
ALTER TABLE `photo`
  MODIFY `id_photo` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `vote`
--
ALTER TABLE `vote`
  ADD CONSTRAINT `photo` FOREIGN KEY (`photo_vote`) REFERENCES `photo` (`id_photo`),
  ADD CONSTRAINT `utilisateur` FOREIGN KEY (`utilisateur_vote`) REFERENCES `utilisateur` (`pseudo_utilisateur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
