    <?php
// on va placer dans ce fichier trois contrôleurs: inscription, connexion, deconnexion
// on charge le modèle contenant les différentes fonctions 
//liés a l'authentification
require ("application/modeles/utilisateurs.php");


function inscription(){
    if(empty($_POST)){
        // si on a reçu aucune donnée via un formulaire, on affiche le formulaire d'inscription 
        require('application/vues/pageInscription.php');
    } else {
        // on verifie que tous les champs sont remplies
        if (empty($_POST['pseudo']) || empty($_POST['mail']) || empty($_POST['pw1']) ||empty($_POST['pw2'])) {
            // on vide le tableau $_POST pour eviter une boucle de redirection infinie
            $_POST = [];
            $_SESSION['error'] = "Merci de renseigner tous les champs";
            header('location:'.baseURL.'index.php?route=inscription');
        }
        //on vérifie la correspondance des deux mots de passe
        if ($_POST['pw1'] != $_POST['pw2']) {
            $_SESSION['error'] = "Les mots de passe ne correspondent pas";
            $_POST =[];
            header('location:'.baseURL.'index.php?route=inscription');
        }
        // on verifie que ni le mail ni le pseudo existe déjà dans la base a l'aide d'une fonction
        if (existeUtilisateur($_POST['pseudo'], $_POST['mail'])) {
            $_SESSION['error'] = "Le pseudo ou le mail existe déjà";
            $_POST = [];
            header('location:'.baseURL.'index.php?route=inscription');
        }
        // on a passé toutes les conditions: on enregistre l'utilsateur dans la base,
        // on effectue la connexion et on redirige vers l'accueil
        $message = inscriptionUtilisateur($_POST['pseudo'], $_POST['mail'], $_POST['pw1']);
        $_SESSION['pseudo'] = $_POST['pseudo'];
        header('location:'.baseURL.'index.php');
    }
}

function connexion(){
    if(empty($_POST)) {
        // si on a recu aucune donnée via un formulaire, on affiche le formulaire de connexion
        require('application/vues/pageConnexion.php');
    } else {
        //on vérifie que tous les champs sont remplis
        if (empty($_POST['pseudo']) || empty($_POST['pw'])) {
            $_POST = [];
            $_SESSION['error'] = "Merci de renseigner tous les champs";
            header('location:'.baseURL.'index.php?route=connexion');
            exit;
        }
        //on utilise le modèle pour verifier le couple login/pw et pour eventuellement connecter l'utilisateur
        if (existeMDP($_POST['pseudo'], $_POST['pw'])) {            
            $_SESSION['pseudo'] = $_POST['pseudo'];
            header('location:'.baseURL.'index.php');
            
        } else {
            $_SESSION['error'] = "Le pseudo ou le mot de passe est incorrect";
            header('location:'.baseURL.'index.php?route=connexion');
            exit;
        }
    }
}

function deconnexion() {
    unset($_SESSION['pseudo']);
    header('location:'.baseURL.'index.php');
}