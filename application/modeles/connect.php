<?php
// fonction de base permettant de se connecter à la base de données
function connect() {
    $userName = 'root';
    $pw = '';
    $dbName = 'authentification';
    try {
        $db = new PDO("mysql:host=localhost;dbname=$dbName",$userName,$pw);
        return $db;
    } catch(PDOException $e) {
        echo "erreur ".$e->getMessage();
        die();
    }
}
?>
