<?php
require('application/modeles/connect.php');
//fonction qui ajoute une photo dans la base de donnée
function ajoutphoto($path, $alt, $titre, $rating) {    
    $db = connect();
    $sql = "INSERT INTO `image`(`imgPath`, `imgAlt`, `imgTitre`, `imgRating`)  VALUES(?, ?, ?, ?)";
    $sth = $db->prepare($sql);
    $sth->execute(array($path, $alt, $titre, $rating));
    $db = null;    
}

