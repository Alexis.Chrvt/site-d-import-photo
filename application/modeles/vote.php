<?php
require('application/modeles/connect.php');

function obtenirVote($id_photo) {

    $dbh = connect();
    $sql =  "SELECT valeur_vote FROM vote WHERE photo_vote=?";
    $sth = $dbh->prepare($sql);
    $sth->execute(array($id_photo, $evaluateur));
    $result = $sth->fetch(PDO::FETCH_ASSOC);

    if (empty($result)) {
        return null;
    }
    return $result['valeur_vote'];
}


function ajoutVoteBDD($id_photo, $note, $auteur) {
        $dbh = connect();
        var_dump($dbh);
    // si une note est déjà présente pour ce couple photo/utilisateur
    // on commence par l'effacer
    if (obtenirVote($id_photo) != null) {
        $sql = "DELETE FROM vote WHERE photo_vote=?";
        $sth = $dbh->prepare($sql);
        $sth->execute(array($id_photo));
    } else {
        // on ajoute la nouvelle valeur dans la base de données
        $sql = "INSERT INTO vote (`photo_vote`, `utilisateur_vote`, `valeur_vote`) VALUES (?, ?, ?)";
        $sth = $dbh->prepare($sql);
        $sth->execute(array($id_photo, $auteur, $note));
    }
}
